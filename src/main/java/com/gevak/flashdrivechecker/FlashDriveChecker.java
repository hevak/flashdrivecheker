package com.gevak.flashdrivechecker;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class FlashDriveChecker {
    private static String os;

    /**
     * This method collect all HDD and Flash Drive device Serial Numbers as String
     * @return collection Serial Numbers
     */
    public static List<String> getSerialNumber() {
        List<String> diskInfo = getDiskInfo();
        if (!diskInfo.isEmpty()) {
            if (os.contains("win")) {
                return getSerialNumbersOnWindows(diskInfo);
            } else if (os.contains("nix") || os.contains("aix") || os.contains("nux")) {
                return getSerialNumbersOnLinux(diskInfo);
            }
        }
        return Collections.emptyList();
    }

    private static List<String> getSerialNumbersOnWindows(List<String> diskInfo) {
        List<String> serialNumber = new ArrayList<>();
        diskInfo.remove(0);
        for (String s : diskInfo) {
            String[] split = s.split(Pattern.quote("\\"));
            serialNumber.add(split[split.length - 1]);
        }
        return serialNumber;
    }

    private static List<String> getSerialNumbersOnLinux(List<String> diskInfo) {
        List<String> serialNumber = new ArrayList<>();
        for (String key : diskInfo) {
            try {
                String s;
                Process p = Runtime.getRuntime().exec("cat /proc/scsi/usb-storage/" + key);
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(p.getInputStream()));
                while ((s = br.readLine()) != null) {
                    if (s.contains("Serial Number")) {
                        String trim = s.split(":")[1].trim();
                        serialNumber.add(trim);
                    }
                }
                p.waitFor();
                p.destroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return serialNumber;
    }

    /**
     * @return Collection of serial numbers as String for Windows and Linux OS
     */
    private static List<String> getDiskInfo() {
        os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            return getDiskInfoWin();
        } else if (os.contains("osx")) {
            throw new UnsupportedOperationException("Operating system Apple OSX based not supported");
        } else if (os.contains("nix") || os.contains("aix") || os.contains("nux")) {
            return getDiskInfoLinux();
        }
        return Collections.emptyList();
    }

    /**
     *  Operating system is based on Linux/Unix/*AIX
     *  - додати до системи репозиторій: sudo add-apt-repository ppa:promasu/libpam-usb
     *  - встановити бібліотеку: sudo apt install libpam-usb
     *  - додати USB-накопичувач: sudo pamusb-conf --add-device NameFlash wmic logicaldisk where drivetype=2 get deviceid
     */
    private static List<String> getDiskInfoLinux() {
        String s;
        List<String> result = new ArrayList<>();
        Process p;
        try {
            p = Runtime.getRuntime().exec("ls /proc/scsi/usb-storage/");
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            while ((s = br.readLine()) != null) {
                result.add(s);
            }
            p.waitFor();
            p.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private static List<String> getDiskInfoWin() {
        List<String> result = new ArrayList<>();
        try {
            Process p = Runtime.getRuntime().exec("wmic diskdrive get PNPDeviceID,InterfaceType");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                if (line.isEmpty()) {
                    continue;
                }
                line = line.trim();
                result.add(line);
            }
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
